%% LyX 2.1.3 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{luainputenc}
\usepackage{float}
\usepackage{graphicx}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% Because html converters don't know tabularnewline
\providecommand{\tabularnewline}{\\}

\makeatother

\usepackage{babel}
\begin{document}

\title{Documentation - Music Game Toolbox}


\author{Lukas Gartlehner, mat\# 0926122}


\date{26. August 2015}

\maketitle

\section{Mapping}

The mapping of events and parameters of the game to audio sources
selected by the player was the main focus of this project. The following
paragraphs discuss the technical aspects as well as the user interface
necessary to accomplish this.


\subsection{Mapping Components}

The Scene Graph architecture of Unity 3D made it necessary to adopt
the class model from the initial concept to a Unity 3D specific model.
All classes in this model implement the interface MonoBehaviour, which
is part of the Unity framework and define GameObject specific behaviors.
Therefor the mapping classes are not instantiated in the regular way
by the C\# ``new'' syntax, instead these classes are added into
the scene graph as script components. This way it is possible to address
other GameObjects at runtime and also make use of inherited MonoBehaviour
methods, e.g. the Start() Method that is called when the scenegraph
is first loaded. Once the package ``Mapping'' is added to a project
any script attached to a Unity GameObject can access the RegistryService
by calling the ``registerEvent'' or ``registerParam'' methods.
Events are simple messages that only consist of an identifier. Once
registered the GameObject can notify the RegistryService at runtime
that the event occurred. If a parameter is registered the RegistryService
can provide a normalized {[}0,1{]} float value for modification of
other parameters. Therefor the GameObject has to provide an interval
for the parameter {[}min, max{]} and has to update the value of the
parameter upon change.

The mapping itself is a triple, consisting of an event, a parameter
and a sound bank {[}e, p, s{]}. The playback of the sound is implemented
in the synthSound class. In this implementation the audio is chosen
from one of ten pre recorded samples of the sample bank, the reasons
for this are discussed in detail in the ``Sound Library'' section.
For user created mappings the class ``menuControls'' creates the
necessary UI elements and delegates the user input to the mapping
components. There are no dependencies between the menuControl component
and the mapping components, therefor the mapping UI is easily interchangeable.
Furthermore the UI for sound mapping could be removed, using an algorithmically
generated sound map.

\begin{figure}[H]
\includegraphics[scale=0.35]{mapping_updated}

\protect\caption{Overview on sound mapping components}


\end{figure}



\subsection{Mapping UI}

The User Interface for mapping is structured in 5 rows that each represent
a mapping entry. Each entry consists of an event (e.g. Enemy drops
a bomb), a parameter (e.g. Smartphone Gyro-Sensor X/Y position) and
a sound bank.

\begin{figure}[H]
\includegraphics[scale=0.28]{02}\protect\caption{In-game UI components for mapping}
\end{figure}


By using the ``Random Map'' button a mapping can be created without
the user having to select a value for each field. The ``Difficulty''
consists of the tempo {[}80, 120{]} and the rhythm (``Techno'',
``HipHop'', ``Jazz''). The faster or more complex (``Techno''
being easiest, ``Jazz'' being hardest) the rhythm of the game progresses,
the harder it gets to play the game, since the events of the game
mechanic (enemy and obstacle movement) are depending on the selected
rhythm. With the use of the ``Music'' button the playback of generated
background music can be changed from normal playback of background
music to the playback of only beats, only melody or no music.


\section{Sound design}

The music content of the game is structured in 3 layers.
\begin{enumerate}
\item Menu music
\item Event based sound from sound banks
\item Generated In-Game background music
\end{enumerate}
Since the menu music is a simple loop of 1:16 min. length there will
be no detailed discussion on this part of the sound design.

All sounds used for this game were created from scratch in Ableton
Live using software synthesizers and self recorded guitar samples.
The only third party sounds in use are single drumhits (Basedrum,
Clap...) from open sound libraries.


\subsection{Mapped Sound}

In the initial concept featured the use of the free library for generic
sound usfxr. During the first testing and bugfixing phase it became
apparent that this library had some severe flaws when used for this
purpose. When creating sound without buffing it will not be possible
to play more than one or two sounds at the same time. Unity 3D is
not capable of using the asynchronous methods usfxr provides without
a freeze in gameplay. If the synchronous method is used, the resulting
sound is cracking or stuttering. To fix this behavior the asynchronous
methods were called at the initialization of the game. 

To limit the buffered sounds to a manageable amount 10 different sound
per preset were created on startup. The input parameter for the sound
was then quantized to 10\% steps. Although the usage of buffered sounds
worked on a technical basis, it made the integration of a library
for generic sound obsolete, since the parameters were predefined on
initialization. Therefor the usfxr library was removed and instead
a set of samples were created in Ableton Live. 

\begin{table}
\begin{tabular}{|c|c|}
\hline 
Sound & Modulation / Description\tabularnewline
\hline 
\hline 
Bass & Synthesizer Bass Sound, CMaj\tabularnewline
\hline 
Bell & Synthesizer Bell Sound, CMaj\tabularnewline
\hline 
Bleepers & Synthesizer Lead Sound with echo, CMaj\tabularnewline
\hline 
Chicago House & Synthesizer Stab Sound with resonator, CMaj\tabularnewline
\hline 
Clap & Snaredrum with low pass filter and delay\tabularnewline
\hline 
Flutter & Percussive sounds with intense delay and echo\tabularnewline
\hline 
Guitar & Guitar score cut into 10 samples\tabularnewline
\hline 
LowEnd & Kickdrum with lowpass filter and delay\tabularnewline
\hline 
Rhythmic & Rhythmic ``shout'' noise in different effect variations\tabularnewline
\hline 
Spheres & Speric pad samples\tabularnewline
\hline 
\end{tabular}

\protect\caption{Overview on sound banks created in Ableton Live}


\end{table}


Each type of sample was used to create a sound bank of ten different
sounds. In some cases (``Bass'', ``Bell'', ``Bleepers'', ``Chicago
House'') the sounds differ in the notes played, other sound banks
(``Clap'', ``Flutter'', ``LowEnd'', ``Rhythmic'') make use
of effects to differentiate the samples. Both the ``Guitar'' and
the ``Spheres'' sound banks make use of different notes, effects
and timbre to differentiate the individual sounds. Depending on the
parameter used in the mapping one of the ten sounds of a sound bank
will be played back in case the mapped event occurs.


\subsection{Background Music}


\subsubsection*{Melody}

The In-Game background music is created algorithmically based on a
markov model approach. In the first step the transition matrices for
note length and note values where created by analysis of Beethovens
``Ode to joy''. In the theme melody the transition between each
note was counted in the first step.

\begin{table}
\begin{tabular}{|c|c|}
\hline 
Note & Transitions\tabularnewline
\hline 
\hline 
C & C A\# C A\# A C A\#\tabularnewline
\hline 
F & F G F G F G G G F G F G F G A\tabularnewline
\hline 
G & F A G A F A F G A A A A F A F\tabularnewline
\hline 
A & A A\# G A G A A\# G G F A\# F A\# F A A\# G G\tabularnewline
\hline 
A\# & C A C A A A C A\tabularnewline
\hline 
\end{tabular}\protect\caption{Occurring notes and notes following (transitions) in Beethovens ``Ode
to joy''}
\end{table}


On the basis of the note count a transitions matrix for the melody
could be created.

\begin{table}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline 
 & C & F & G & A & A\# & Count\tabularnewline
\hline 
\hline 
C & $\frac{3}{7}$ & 0 & 0 & $\frac{1}{7}$ & $\frac{3}{7}$ & 7\tabularnewline
\hline 
F & 0 & $\frac{6}{15}$ & $\frac{8}{15}$ & $\frac{1}{15}$ & 0 & 15\tabularnewline
\hline 
G & 0 & $\frac{5}{15}$ & $\frac{3}{15}$ & $\frac{8}{15}$ & 0 & 15\tabularnewline
\hline 
A & 0 & $\frac{3}{18}$ & $\frac{6}{18}$ & $\frac{4}{18}$ & $\frac{5}{18}$ & 18\tabularnewline
\hline 
A\# & $\frac{3}{8}$ & 0 & 0 & $\frac{5}{8}$ & 0 & 8\tabularnewline
\hline 
\end{tabular}\protect\caption{Transition matrix for the Markov Model}
\end{table}


The same process of analysis was done for the length of notes played
in the melody. Furthermore a third transition matrix was created that
was used to switch between banks of samples that differed in sound.
Unfortunately the output of sound based on this model could hardly
be called music. Step by step the matrix was simplified to make the
output more appealing to the player. One aspect of this simplification
involved leading back to the note ``C'' with highest probability
from all other states. Another change was made so that the duration
of notes also included a pause state, since the initial model did
not make use of the pause notation. Furthermore the output of the
model was buffered for 32 bars and for the music styles of ``Techno''
and ``HipHop'' a 8 bar / 16 bar sequence of the melody is repeated
inside these 32 bars to create a repeating melodic theme. For the
style ``Jazz'' this feature is not active.

Each of the three music styles (``Techno'', ``HipHop'', ``Jazz'')
was assigned a slightly different model, so that the generated melody
gets more or less complex.


\subsubsection*{Rhythm}

For each of the music styles a basic beat pattern was created that
represents the stereotypical bassdrum and snaredrum pattern of each
style. To add a dynamic component to the beat in each level the percussive
part of the beat is created by a simple randomization. In a 64 beats
pattern the probability of a note being one of two percussion hits
is 10\% each, which leaves another 80\% chance for this note to be
a pause. For ``HipHop'' a sub pattern repeats 4 times, for ``Techno''
the repetition is 8 times. On the start of each level a new percussion
pattern is generated.


\section{Graphic Redesign}

First tests of gamplay showed that users had difficulties identifying
the different elements of the gameplay when playing the game on a
smartphone, mostly due to the small size of game elements on the handheld
screen. Therefor all sprites and background images were changed to
more distinguishable game elements, bigger in size and with more detailed
textures. All sprites and backgrounds were created from scratch in
Adobe Photoshop. Similar to the sound design there was no third party
material used in the creation of the game. By using the pixel-art
technique it was possible to create texture and shading pixel by pixel.

\begin{table}[H]
\begin{tabular}{|c|c|c|}
\hline 
Old Sprite & Redesign & Description\tabularnewline
\hline 
\hline 
\includegraphics{old_sprites/obstacle} & \includegraphics[scale=0.8]{sprite_rework/comet} & Obstacle (Comet)\tabularnewline
\hline 
\includegraphics{old_sprites/bomb} & \includegraphics[bb=100bp 80bp 150bp 124bp,scale=0.8]{sprite_rework/rocket} & Projectile (Bomb)\tabularnewline
\hline 
\includegraphics{old_sprites/brick} & \includegraphics[bb=50bp 20bp 182bp 124bp,scale=0.6]{sprite_rework/satellite1} & Collectible points (Satellite)\tabularnewline
\hline 
\includegraphics{old_sprites/ship} & \includegraphics[scale=0.8]{sprite_rework/ship} & Player Ship\tabularnewline
\hline 
\includegraphics{old_sprites/enemy} & \includegraphics[scale=0.8]{sprite_rework/enemy} & Enemy Ship\tabularnewline
\hline 
\end{tabular}

\protect\caption{Sprites for game objects before and after redesign}


\end{table}


Furthermore, the background consists of four separate layers, two
of which are ``planets'' and the other two ``stars''. The opacity
of the background layers changes in accordance to the speed set by
the player. This simple visual effect is supposed to underline the
rhythmic component of the game. In order to give the game an appealing,
state of the art look, particle effects and explosions were added
to the game objects. This way the static, 2-dimensional objects gain
visual depth. The screens below show the gameplay in comparison.

\begin{figure}[H]
\includegraphics[bb=0bp 30bp 510bp 360bp,clip,scale=0.7]{solar_fox_enhanced}

\protect\caption{Screen of initial gameplay}
\end{figure}


\begin{figure}[H]
\includegraphics[scale=0.28]{01}\protect\caption{Screen after redesign}
\end{figure}



\section{User Feedback}


\subsection{Incremental feedback}

In the course of the development the game was presented to various
people in order to collect informal feedback. Many of the aspects
of the graphic redesign where due to the feedback on the early prototype.
The users where asked to play the game, but had problems distinguishing
the game elements and figuring out the goal of the game. 

After the graphic redesign the game prototype was controlled by tilting
the phone. The sensory data was used to increase movement of the starship
towards the lowest corner. Although the concept of using sensors sounded
promising in theory, most users where unable to cope with the requirement
of a very steady hand in order to play a mere casual game. This finding
lead to the use of the touchscreen for controls, moving the starship
towards the fingertip of the player.

Since the overall goal is to use the game as a source of generated
sound the settings menu was redesigned as well. The font and font
size was changed to make the buttons easier to read. Furthermore the
size and position of the buttons was adopted to fit the screen dimensions.


\subsection{Beta Test}

To gain a broader feedback the beta test functionality of the Google
Play Store was used. On a functional level no crashes where reported.
The devices of the beta tester who returned the feedback questionnaire
where Sony z1 compact, Motorola Moto G, Motorola Moto X, Samsung Galaxy
Ace Core and the Fairphone. Since the game runs in full screen mode
some beta users mentioned that they wished for an exit button in the
game. Unity 3d shows the system controls of Android after swiping
from the top down, but without knowing this it is not easy to find
a way to exit the game. Another UI related aspect of the feedback
was that the controls, which steer the ship towards the finger on
the screen, are not ideal since the finger sometimes covers game elements,
which makes it hard to play.

The most valuable feedback was that the mapping of sound was to complex
to grasp in the context of a casual game. Users reported that they
found out how to change the sound settings, but that is was not quite
easy to find that out without help. The quality of sound was perceived
very positively by the participants, however it was hard for most
beta testers to find a suitable setting which was not stressful after
a while. 


\subsection{Outlook for future development}

What the beta test and a final informal interview revealed is that,
besides smaller problems with the controls, the concept is to broad
for the platform of mobile games. The game would work as a simple
casual game, if the soundmapping would be left out. But with the option
to map sounds the complexity of the game is to high to fit the platform.
One way to overcome this problem would be to create profiles of soundmaps
that could be used and altered later on. This way only those with
a deeper interest in the mapping capabilities would need to deal with
this aspect, all other users could ignore this feature entirely. 

The other option, is to switch to the PC as a platform, which would
allow the user to master more complex menus. Therefor the game could
be adjusted to run on a PC and played in the context of an event.
In this live version of the game the sound would not pause for any
game action. Furthermore it would not be possible to adjust the settings.
The aim of such a version of the game would be to be played in a room
full of people, creating music as it is played - as an interactive
live installation.


\section{Release and reuse}

The game is available on Google Play Store under the name of ``zeroG
Waves''. Currently only the beta can be accessed via

https://play.google.com/store/apps/details?id=at.locoPxl.zeroG

To meet the minimum standards for a published app an app icon, screenshots
and functional graphics were created. In the game itself the user
gets to a start screen after launching the app. This startscreen lets
the user choose settings before starting the actual and also offers
a manual though a one-page help.

\begin{table}[H]
\includegraphics[scale=0.1]{H:/LocalWorkspace/FromDesignToSW/Unity/Assets/app_icon/app_icon}

\protect\caption{App icon}


\end{table}


\begin{figure}[H]
\includegraphics[scale=0.35]{Screenshots/function}

\protect\caption{Function graphic}


\end{figure}


The source of the game can be downloaded from a public Git repository
on BitBucket by anyone who is interested in aspects of this work:

https://bitbucket.org/lgart/fromdesigntosoftware/src


\section{Next steps - From Design To Software 2}

As mentioned in the User Feedback section the transformation to a
desktop game would be an option worth exploring. For the course ``From
Design To Software 2'' it would be possible to reuse the mapping
components, which were described in the ``Mapping'' section of this
paper. The context of a live installation where people can compete
against each other while creating music with their gameplay would
be a setting worth exploring. One of the ideas that came up during
interviews with a beta tester was to make a round base game concept,
where each round has the length of a certain amount of beats (e.g.
one bar) and each action a player takes is quantized into the rhythm.
By assigning sounds to these actions this game would also create music
based on the gameplay. 
\end{document}
