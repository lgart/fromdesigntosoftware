%% LyX 2.1.3 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[utf8]{luainputenc}
\usepackage{float}
\usepackage{graphicx}
\usepackage{babel}
\begin{document}

\title{Design Overview - Music Game Toolbox}


\author{Lukas Gartlehner, mat\# 0926122}


\date{20. Feb. 2015}

\maketitle

\section{Basic Concept}

In the course of the project ``From Design to Software 1 - PR''
a videogame will be developed. However, the game itself will be the
mere proof of concept for the usage of a game-engine and the capabilities
for user interface design that it provides as a music interface. The
core idea is to design and develop the game in a matter so that its
components can be reused to build music interfaces. For the purpose
of possible reuse as a music interface the game will be developed
for mobile platforms. 

For the concept proving game a selected number of controls and elements
of the game mechanic will be mapped to tone generating modules. The
game will generate sound during game play by using synthesizer elements
like oscillators and filters instead of predefined audio samples.
Furthermore, the characteristics of these sounds will vary in relation
to the state of the game and its parameters. 


\subsection{Refactoring}

For the course ``Exploratives Design'' a minigame for the desktop
was developed. This game is a clone of the Atari game Solar Fox \cite{SolarFox},
but was enhanced in terms of graphics and game mechanics as part of
an exercise. For the music game this will be used as a code and asset
base. The game has to be refactored so that the elements relevant
for sound creation and manipulation are clearly distinguishable form
all elements that implement the game mechanic itself. 


\subsection{Device optimization}

Further, the user input and display optimization have to be adjusted
so that the desktop game can be played on a mobile device. The target
device will be a Android based Smartphone or Tablet. As a minimum
requirements we define Android 4.4 ``KitKat'' as an operating system
and a minimum device resolution HD-ready 1280 x 720 as well as OpenGL
ES 3.0 support, which is already implicitly defined by the OS requirement
\cite{AndroidVersions}. It is maybe possible to run the game on devices
below these requirements, however, there will be no optimization for
devices that do not meet this criteria.


\subsection{Game principles}

The game mechanics are based on the Atari game Solar Fox. In this
game the player steers a little spaceship in x and y direction on
the screen. The player can pick up the rectangular elements, which
will result in points gained. When all points of a level were collected
the level is completed and a new level with points to pick up is loaded.
The enemy ships on the top and bottom of the screen shoot bombs to
the opposite side of the screen. If the ship collides with such a
bomb the player looses a life.

\begin{figure}[H]
\includegraphics[scale=0.8]{solar_fox_original}

\protect\caption{Atari game Solar Fox, original screen}


\end{figure}


Based on this minigame an enhanced version was created. Besides the
added background image and higher resolution the game also has slightly
changed game mechanics. The small lines are obstacles which will cost
the player a life in case of collision. Furthermore, the player can
choose to rotate all obstacles by 90 degrees. This allows to get to
all points of the level, but has to be handled with care, because
the rotation itself can also result in collision. 

\begin{figure}[H]
\includegraphics[scale=0.45]{solar_fox_enhanced}

\protect\caption{Unity game based on Solar Fox, Enemy game objects on top and bottom}


\end{figure}


The bombs dropped by the enemy ships can also destroy the obstacles.
The player can further choose to rotate the enemy ships from top and
bottom to the left and right of the screen. After rotating the enemy
ships the bombs fly on the horizontal axis, destroying vertical obstacles.

\begin{figure}[H]
\includegraphics[scale=0.45]{solar_fox_enhanced_rotate}\protect\caption{Unity game based on Solar Fox, Enemy game objects on left and right,
obstacles in rotation}


\end{figure}


To adopt these game mechanics to the project instead of using the
arrow keys to steer the spaceship, the x and y parameter of the gyroscope
will be used to steer the ship. So by tilting the smartphone or tablet
the ships will accelerate into this direction. The option of rotation
for both the obstacles and enemy ships will not be available to the
user anymore. Rotations will be done automatically and are part of
the difficulty level. The difficulty of the game can be chosen by
the player in an intial menu and consist of two parameters
\begin{itemize}
\item Speed
\item Rhythm
\end{itemize}
The user will be able to set the speed in beats per minute (bpm) to
change the difficulty of the game. Easy gameplay is located at 80
bpm, the very hard settings go up to 170 bpm. As a second parameter
the rhythm can be adjusted. All actions of the gameplay follow this
rhythm in the chosen speed. Enemy ships and obstacles will rotate
based on these parameters, also the dropping of bombs will follow
the rhythm. One of three rhythmic patterns can be selected by the
user, the easiest being a ``Four to the floor'' pattern, with a
base hit on every quater note. More complex rhythms will have eighth
notes and several bars of alternating drum hits, to make the movement
of obstacles and enemy ships harder to predict. However, all rhythms
will be based on a four-four time.


\subsection{Audio Output}

For the demo game the proof of concept is to enable the user to create
music while playing. Since all elements of the game rely on the same
basic speed and rhythm, the events in the game create the frame for
music to be generated. In the initial dialog the user can not only
set what speed and rhythm the underlying game elements should use,
it is also possible to map these actions with sound sources. The basic
input parameters for this purpose will be:
\begin{itemize}
\item Rotation of obstacles
\item Rotation of enemy ships
\item Drop of bomb
\item x/y position of the spaceship
\item x/y tilt of the device
\end{itemize}
This list is a starting point for exploration. Depending on tests
with the first prototype it will then be possible to identify what
parameters and sound sources are most suitable to create music by
playing.

\begin{figure}[H]
\includegraphics[scale=0.5]{mapping_menu_cut}

\protect\caption{Mockup of setup screen}


\end{figure}


These parameters can be mapped to sound generating elements (oscillators)
and sound shaping elements (filters and effects). Momentary actions
(dropping a bomb) can be mapped to either a single note being played
or a random note from a scale. Constant values (x/y positions) can
be mapped to continuous parameters (filter frequency, oscillator frequency,
effect amount).


\subsection{Software Architecture}


\subsubsection{SDK and Modules}

The game engine Unity 3D will be used as a framework for the creation
of the game. Unity 3D offers a full SDK that provides tools for scene
generation, coding and compiling. It is possible to compile the game
for various platforms (Desktop, Web, iOS, Android), however, as stated
in 1.2 only specific Android devices will be targeted for this project
\cite{Unity3d}. The assets necessary for the game will be created
an modified with Adobe Photoshop and GIMP 2.0.

Since Unity 3D can only play audio samples, but has no capabilities
to create dynamic sound, additional modules will be used. For the
basic sound generated by the game the library ``usfxr - Procedural
Sound Effects'' will be included in the project \cite{usfxr}. usfxr
is a free module for audio scripting that allows simple audio DSP
programming in Unity 3D. Further, we will explore the option of adding
depth and enhanced modulation to the generated sound by using the
free libraries ``Sound Filters and Effects'' \cite{SoundFilters}
and ``Audial Manipulators''\cite{Audial}.


\subsubsection{Reuseable Components}

To emphasize the reusable character of the game developed the design
will be split in 3 layers. This delegation of responsibilities can
be compared to the Model View Controller paradigm \cite{mvc} in object
oriented programming. We propose a 3 layer model that consists of
a control, a mapping and an audio layer.


\paragraph{Control Layer}

On this level all relevant parameters that are supposed to control
the sound generating elements are defined. This will most likely be
the input of a user, in this specific case it could be the gyroscopes
x/y-Axis or the position of the users finger on the screen. Furthermore,
this could also be the x/y/z position of any Unity game object.


\paragraph{Mapping Layer}

This layer should hold the logic of mapping control layer data to
sound sources. Incoming values might need to be rounded, quantized
or normalized to be in a certain useable range. 


\paragraph{Audio Layer}

After scaling the values, the mapping layer delivers control data
to the audio layer. This layer produces sound based on the information
provided by the control and mapping layer. The audio layer should
be interchangeable, so ideally the mapping layer delivers values in
the range of MIDI. In a future version this layer could be located
on another device, making it possible to control a virtual synthesizer
or a digital audio workstation with a game or game like interface.

Since Unity 3D attaches scripts to components in the scene / scene
graph it may not be possible to split the responsibilities of the
control and mapping layer entirely from the game mechanics. However,
the goal is to keep the audio layer separated as much as possible,
so it will be interchangeable.


\section{Project Management}


\subsection{Versioning}

Unity 3D applications can be developed by writing code in boo, JavaScript
and C\#. Since many libraries use C\# and C\# is a well structured,
object oriented language, the scripts will be written in C\#. These
scripts can be saved in external files, making it possible to track
them with the version control tool Git. The scene graph and the assets
however are binary data, so only limited versioning is possible for
those elements of the game.

Tthe local Git repository will be pushed to a remote master branch
on BitBucket. This remote repository also acts as a backup in case
of data loss.


\subsection{Licenses}

The Unity 3D game engine and SDK are free as long as no Unity Pro
version is needed and it is used in a non-profit context. Git and
BitBucket can also be used without cost. Furthermore, modules we plan
to integrate are also free.


\subsection{Timeframe}

\begin{figure}[H]
\includegraphics[scale=0.31]{gant_project}

\protect\caption{Preliminary timeframe in Gantt notation}


\end{figure}


After the initial refactoring of the existing game we plan to work
on implementing the basic features in three weeks. We aim to produce
a very basic alpha version by the 20th of March. This version will
allow thorough testing and bug fixing. By the end of March a stable
Beta version should be available. This version will then be reviewed
and if necessary changes will be made.


\subsection{Exploration}

Many of the details necessary to implement this project are not defined
in this paper. Since this is not a business application with clearly
definable goals and features, it is yet unclear how to implement functions
like the mapping of parameters or the adjustable parameters of the
sound module. These details can only be set after an initial prototype
has been created and the parameters can be adjusted and debugged.
For those reasons these details remain uncharted areas on the roadmap
of the project and will be explored in the process of development.


\section{Research Objective}

The overall goal of this project is to evaluate which mapping of parameters
creates the most immersive and coherent game experience. The prototype
should encourage the user to play around with various settings and
create his or her own soundtrack while playing the game. Since the
quality of game music and soundtracks is subjective it seems logic
to approach this problem on an empiric level. Due to the fact that
a comprehensive useability study would go beyond the scope of this
project we choose the approach of hallway testing. The prototype will
be installed on a phone or tablet and handed to volunteers with the
request to play the game for 10 minutes and further to find a mapping
that suits his or her gameplay experience the most. By comparing the
settings of at least 10 participants we hope to identify tendencies
that can be used in future development to create immersive and dynamic
in game sound.


\section{Further Development}

Based on the findings of this project it is possible to create applications
for mobile devices that can be used for music production or for automation
in general. One of the most successful examples of such an application
is Lemur \cite{Lemur}, which enables the user to map controls of
digital music production to touch optimized interfaces. This could
be done by removing the audio layer from the application itself and
creating a proxy instead that sends that sends MIDI or OSC control
data to a receiver application on a digital audio workstation. The
advantage of this project, compared to Lemur, is that it could make
use of 3D and physics capabilities of Unity 3D, while being easily
deployed on various mobile platforms. 

A different angle on further development would be to use the mapping
and audio layers on a different game. In this context it would be
easily possible to create dynamic sound that is interconnected to
the game mechanics.

\bibliographystyle{IEEEtran}
\bibliography{/cygdrive/C/LocalWorkspace/SeminarMedInf/FDTS}

\end{document}
