#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Design Overview - Music Game Toolbox
\end_layout

\begin_layout Author
Lukas Gartlehner, mat# 0926122
\end_layout

\begin_layout Date
20.
 Feb.
 2015
\end_layout

\begin_layout Section
Basic Concept
\end_layout

\begin_layout Standard
In the course of the project 
\begin_inset Quotes eld
\end_inset

From Design to Software 1 - PR
\begin_inset Quotes erd
\end_inset

 a videogame will be developed.
 However, the game itself will be the mere proof of concept for the usage
 of a game-engine and the capabilities for user interface design that it
 provides as a music interface.
 The core idea is to design and develop the game in a matter so that its
 components can be reused to build music interfaces.
 For the purpose of possible reuse as a music interface the game will be
 developed for mobile platforms.
 
\end_layout

\begin_layout Standard
For the concept proving game a selected number of controls and elements
 of the game mechanic will be mapped to tone generating modules.
 The game will generate sound during game play by using synthesizer elements
 like oscillators and filters instead of predefined audio samples.
 Furthermore, the characteristics of these sounds will vary in relation
 to the state of the game and its parameters.
 
\end_layout

\begin_layout Subsection
Refactoring
\end_layout

\begin_layout Standard
For the course 
\begin_inset Quotes eld
\end_inset

Exploratives Design
\begin_inset Quotes erd
\end_inset

 a minigame for the desktop was developed.
 This game is a clone of the Atari game Solar Fox 
\begin_inset CommandInset citation
LatexCommand cite
key "SolarFox"

\end_inset

, but was enhanced in terms of graphics and game mechanics as part of an
 exercise.
 For the music game this will be used as a code and asset base.
 The game has to be refactored so that the elements relevant for sound creation
 and manipulation are clearly distinguishable form all elements that implement
 the game mechanic itself.
 
\end_layout

\begin_layout Subsection
Device optimization
\end_layout

\begin_layout Standard
Further, the user input and display optimization have to be adjusted so
 that the desktop game can be played on a mobile device.
 The target device will be a Android based Smartphone or Tablet.
 As a minimum requirements we define Android 4.4 
\begin_inset Quotes eld
\end_inset

KitKat
\begin_inset Quotes erd
\end_inset

 as an operating system and a minimum device resolution HD-ready 1280 x
 720 as well as OpenGL ES 3.0 support, which is already implicitly defined
 by the OS requirement 
\begin_inset CommandInset citation
LatexCommand cite
key "AndroidVersions"

\end_inset

.
 It is maybe possible to run the game on devices below these requirements,
 however, there will be no optimization for devices that do not meet this
 criteria.
\end_layout

\begin_layout Subsection
Game principles
\end_layout

\begin_layout Standard
The game mechanics are based on the Atari game Solar Fox.
 In this game the player steers a little spaceship in x and y direction
 on the screen.
 The player can pick up the rectangular elements, which will result in points
 gained.
 When all points of a level were collected the level is completed and a
 new level with points to pick up is loaded.
 The enemy ships on the top and bottom of the screen shoot bombs to the
 opposite side of the screen.
 If the ship collides with such a bomb the player looses a life.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename solar_fox_original.png
	scale 80

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Atari game Solar Fox, original screen
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
Based on this minigame an enhanced version was created.
 Besides the added background image and higher resolution the game also
 has slightly changed game mechanics.
 The small lines are obstacles which will cost the player a life in case
 of collision.
 Furthermore, the player can choose to rotate all obstacles by 90 degrees.
 This allows to get to all points of the level, but has to be handled with
 care, because the rotation itself can also result in collision.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename solar_fox_enhanced.png
	scale 45

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Unity game based on Solar Fox, Enemy game objects on top and bottom
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
The bombs dropped by the enemy ships can also destroy the obstacles.
 The player can further choose to rotate the enemy ships from top and bottom
 to the left and right of the screen.
 After rotating the enemy ships the bombs fly on the horizontal axis, destroying
 vertical obstacles.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename solar_fox_enhanced_rotate.png
	scale 45

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Unity game based on Solar Fox, Enemy game objects on left and right, obstacles
 in rotation
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
To adopt these game mechanics to the project instead of using the arrow
 keys to steer the spaceship, the x and y parameter of the gyroscope will
 be used to steer the ship.
 So by tilting the smartphone or tablet the ships will accelerate into this
 direction.
 The option of rotation for both the obstacles and enemy ships will not
 be available to the user anymore.
 Rotations will be done automatically and are part of the difficulty level.
 The difficulty of the game can be chosen by the player in an intial menu
 and consist of two parameters
\end_layout

\begin_layout Itemize
Speed
\end_layout

\begin_layout Itemize
Rhythm
\end_layout

\begin_layout Standard
The user will be able to set the speed in beats per minute (bpm) to change
 the difficulty of the game.
 Easy gameplay is located at 80 bpm, the very hard settings go up to 170
 bpm.
 As a second parameter the rhythm can be adjusted.
 All actions of the gameplay follow this rhythm in the chosen speed.
 Enemy ships and obstacles will rotate based on these parameters, also the
 dropping of bombs will follow the rhythm.
 One of three rhythmic patterns can be selected by the user, the easiest
 being a 
\begin_inset Quotes eld
\end_inset

Four to the floor
\begin_inset Quotes erd
\end_inset

 pattern, with a base hit on every quater note.
 More complex rhythms will have eighth notes and several bars of alternating
 drum hits, to make the movement of obstacles and enemy ships harder to
 predict.
 However, all rhythms will be based on a four-four time.
\end_layout

\begin_layout Subsection
Audio Output
\end_layout

\begin_layout Standard
For the demo game the proof of concept is to enable the user to create music
 while playing.
 Since all elements of the game rely on the same basic speed and rhythm,
 the events in the game create the frame for music to be generated.
 In the initial dialog the user can not only set what speed and rhythm the
 underlying game elements should use, it is also possible to map these actions
 with sound sources.
 The basic input parameters for this purpose will be:
\end_layout

\begin_layout Itemize
Rotation of obstacles
\end_layout

\begin_layout Itemize
Rotation of enemy ships
\end_layout

\begin_layout Itemize
Drop of bomb
\end_layout

\begin_layout Itemize
x/y position of the spaceship
\end_layout

\begin_layout Itemize
x/y tilt of the device
\end_layout

\begin_layout Standard
This list is a starting point for exploration.
 Depending on tests with the first prototype it will then be possible to
 identify what parameters and sound sources are most suitable to create
 music by playing.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename mapping_menu_cut.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Mockup of setup screen
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
These parameters can be mapped to sound generating elements (oscillators)
 and sound shaping elements (filters and effects).
 Momentary actions (dropping a bomb) can be mapped to either a single note
 being played or a random note from a scale.
 Constant values (x/y positions) can be mapped to continuous parameters
 (filter frequency, oscillator frequency, effect amount).
\end_layout

\begin_layout Subsection
Software Architecture
\end_layout

\begin_layout Subsubsection
SDK and Modules
\end_layout

\begin_layout Standard
The game engine Unity 3D will be used as a framework for the creation of
 the game.
 Unity 3D offers a full SDK that provides tools for scene generation, coding
 and compiling.
 It is possible to compile the game for various platforms (Desktop, Web,
 iOS, Android), however, as stated in 1.2 only specific Android devices will
 be targeted for this project 
\begin_inset CommandInset citation
LatexCommand cite
key "Unity3d"

\end_inset

.
 The assets necessary for the game will be created an modified with Adobe
 Photoshop and GIMP 2.0.
\end_layout

\begin_layout Standard
Since Unity 3D can only play audio samples, but has no capabilities to create
 dynamic sound, additional modules will be used.
 For the basic sound generated by the game the library 
\begin_inset Quotes eld
\end_inset

usfxr - Procedural Sound Effects
\begin_inset Quotes erd
\end_inset

 will be included in the project 
\begin_inset CommandInset citation
LatexCommand cite
key "usfxr"

\end_inset

.
 usfxr is a free module for audio scripting that allows simple audio DSP
 programming in Unity 3D.
 Further, we will explore the option of adding depth and enhanced modulation
 to the generated sound by using the free libraries 
\begin_inset Quotes eld
\end_inset

Sound Filters and Effects
\begin_inset Quotes erd
\end_inset

 
\begin_inset CommandInset citation
LatexCommand cite
key "SoundFilters"

\end_inset

 and 
\begin_inset Quotes eld
\end_inset

Audial Manipulators
\begin_inset Quotes erd
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Audial"

\end_inset

.
\end_layout

\begin_layout Subsubsection
Reuseable Components
\end_layout

\begin_layout Standard
To emphasize the reusable character of the game developed the design will
 be split in 3 layers.
 This delegation of responsibilities can be compared to the Model View Controlle
r paradigm 
\begin_inset CommandInset citation
LatexCommand cite
key "mvc"

\end_inset

 in object oriented programming.
 We propose a 3 layer model that consists of a control, a mapping and an
 audio layer.
\end_layout

\begin_layout Paragraph
Control Layer
\end_layout

\begin_layout Standard
On this level all relevant parameters that are supposed to control the sound
 generating elements are defined.
 This will most likely be the input of a user, in this specific case it
 could be the gyroscopes x/y-Axis or the position of the users finger on
 the screen.
 Furthermore, this could also be the x/y/z position of any Unity game object.
\end_layout

\begin_layout Paragraph
Mapping Layer
\end_layout

\begin_layout Standard
This layer should hold the logic of mapping control layer data to sound
 sources.
 Incoming values might need to be rounded, quantized or normalized to be
 in a certain useable range.
 
\end_layout

\begin_layout Paragraph
Audio Layer
\end_layout

\begin_layout Standard
After scaling the values, the mapping layer delivers control data to the
 audio layer.
 This layer produces sound based on the information provided by the control
 and mapping layer.
 The audio layer should be interchangeable, so ideally the mapping layer
 delivers values in the range of MIDI.
 In a future version this layer could be located on another device, making
 it possible to control a virtual synthesizer or a digital audio workstation
 with a game or game like interface.
\end_layout

\begin_layout Standard
Since Unity 3D attaches scripts to components in the scene / scene graph
 it may not be possible to split the responsibilities of the control and
 mapping layer entirely from the game mechanics.
 However, the goal is to keep the audio layer separated as much as possible,
 so it will be interchangeable.
\end_layout

\begin_layout Section
Project Management
\end_layout

\begin_layout Subsection
Versioning
\end_layout

\begin_layout Standard
Unity 3D applications can be developed by writing code in boo, JavaScript
 and C#.
 Since many libraries use C# and C# is a well structured, object oriented
 language, the scripts will be written in C#.
 These scripts can be saved in external files, making it possible to track
 them with the version control tool Git.
 The scene graph and the assets however are binary data, so only limited
 versioning is possible for those elements of the game.
\end_layout

\begin_layout Standard
Tthe local Git repository will be pushed to a remote master branch on BitBucket.
 This remote repository also acts as a backup in case of data loss.
\end_layout

\begin_layout Subsection
Licenses
\end_layout

\begin_layout Standard
The Unity 3D game engine and SDK are free as long as no Unity Pro version
 is needed and it is used in a non-profit context.
 Git and BitBucket can also be used without cost.
 Furthermore, modules we plan to integrate are also free.
\end_layout

\begin_layout Subsection
Timeframe
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename gant_project.png
	scale 31

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Preliminary timeframe in Gantt notation
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
After the initial refactoring of the existing game we plan to work on implementi
ng the basic features in three weeks.
 We aim to produce a very basic alpha version by the 20th of March.
 This version will allow thorough testing and bug fixing.
 By the end of March a stable Beta version should be available.
 This version will then be reviewed and if necessary changes will be made.
\end_layout

\begin_layout Subsection
Exploration
\end_layout

\begin_layout Standard
Many of the details necessary to implement this project are not defined
 in this paper.
 Since this is not a business application with clearly definable goals and
 features, it is yet unclear how to implement functions like the mapping
 of parameters or the adjustable parameters of the sound module.
 These details can only be set after an initial prototype has been created
 and the parameters can be adjusted and debugged.
 For those reasons these details remain uncharted areas on the roadmap of
 the project and will be explored in the process of development.
\end_layout

\begin_layout Section
Research Objective
\end_layout

\begin_layout Standard
The overall goal of this project is to evaluate which mapping of parameters
 creates the most immersive and coherent game experience.
 The prototype should encourage the user to play around with various settings
 and create his or her own soundtrack while playing the game.
 Since the quality of game music and soundtracks is subjective it seems
 logic to approach this problem on an empiric level.
 Due to the fact that a comprehensive useability study would go beyond the
 scope of this project we choose the approach of hallway testing.
 The prototype will be installed on a phone or tablet and handed to volunteers
 with the request to play the game for 10 minutes and further to find a
 mapping that suits his or her gameplay experience the most.
 By comparing the settings of at least 10 participants we hope to identify
 tendencies that can be used in future development to create immersive and
 dynamic in game sound.
\end_layout

\begin_layout Section
Further Development
\end_layout

\begin_layout Standard
Based on the findings of this project it is possible to create applications
 for mobile devices that can be used for music production or for automation
 in general.
 One of the most successful examples of such an application is Lemur 
\begin_inset CommandInset citation
LatexCommand cite
key "Lemur"

\end_inset

, which enables the user to map controls of digital music production to
 touch optimized interfaces.
 This could be done by removing the audio layer from the application itself
 and creating a proxy instead that sends that sends MIDI or OSC control
 data to a receiver application on a digital audio workstation.
 The advantage of this project, compared to Lemur, is that it could make
 use of 3D and physics capabilities of Unity 3D, while being easily deployed
 on various mobile platforms.
 
\end_layout

\begin_layout Standard
A different angle on further development would be to use the mapping and
 audio layers on a different game.
 In this context it would be easily possible to create dynamic sound that
 is interconnected to the game mechanics.
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "C:/LocalWorkspace/SeminarMedInf/FDTS"
options "IEEEtran"

\end_inset


\end_layout

\end_body
\end_document
