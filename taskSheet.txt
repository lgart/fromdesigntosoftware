
CURRENT:


*Abschluss & Vorbereitung Folgeprojekt*
* Workshop mit Feedback zu dieser Version
* Abschlusspaper bestehend aus
     * Dokumentation des Endzustands
     * Resumee des Workshops 
     * "Pitfalls" bei der Entwicklung
	
* Upload in Store
	

	
IN PROGRESS:

* App Icon





	

	
	
DONE:
* Weniger Hindernisse & gegnerische Angriffe -> bisher zu schwierig
* Steuerung ändern - per Touch-Geste statt über Neigungswinkel
     * Neigungswinkel bleibt als Parameter für Sound erhaltentr
* Mehr Auswahl im Mapping (zusätzliche Events und Mod-Parameter)
* Anpassen der Pattern (Techno, HipHop, Jazz) an das Gameplay

* Algorithmisch generierte Musik als Hintergrundsound
     * Markov Ketten steuern Tonhöhe, Tonlänge und Klangfarbe
	 
	 - Markov Model Buffering (32 Steps)
	 - Tonhöhe dynamischer Sounds anpassen
	 
	 - UI to Set Dynamic Music / Beat  off
	 
* New percussion pattern each round

* Enemy Rotation as Parameter

* Flickering Background

* Startscreen & Menü überarbeiten (Buttons sind tlw. am Smartphone zu klein)
* Font & ButtonStyle
* Bug -> HipHop -> Enemy locked
* Random Button
* Pad statt Guitar
* BG Music for menu




