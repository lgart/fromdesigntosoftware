﻿using UnityEngine;
using System.Collections;

public class markovChains : MonoBehaviour {

	float[][] noteChain;
	float[][] lengthChain;
	float[][] soundChain;

	int[] curState;
	int stepCounter=0;

	int[][] bufferedChain;

	string[] sNote = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "H"};
	string[] sLength = {"1", "2", "4", "8", "16"};
	string[] sSound = {"markov_notes_1", "markov_notes_2", "Sound 3", "Sound 4", "Pause"};
	string musicStyle="";

	private AudioClip[] audioClips;
	private AudioSource audioSource;
	private string cachedSoundName="";
	
	
	// Use this for initialization
	void Start () {
		audioClips = new AudioClip[1];
		audioSource = gameObject.AddComponent<AudioSource>();

		setChains ("Techno");
		bufferTransitions();
	}



	public void transition() {

		curState[0] = bufferedChain[0][stepCounter];
		curState[1] = bufferedChain[1][stepCounter];
		curState[2] = bufferedChain[2][stepCounter++];

		Debug.Log ("Note: " + sNote [curState [0]] + " - Length: " + sLength [curState [1]] + " - Sound: " + sSound [curState [2]]);

		if (curState [1] != 0) {
			cachedSoundName = sSound [curState [2]] + "/" + sNote [curState [0]] + "_" + sLength [curState [1]];
			audioClips [0] = (AudioClip)Resources.Load (cachedSoundName) as AudioClip;
			audioSource.PlayOneShot (audioClips [0]);
		}

		if (stepCounter == 32) {
			bufferTransitions();
			stepCounter=0;
		}

	}

	/*
	 * buffers 32 steps of a markov matrix
	 */
	private void bufferTransitions() {
		bufferedChain = new int[3][];

		bufferedChain [0] = new int[32]; //note
		bufferedChain [1] = new int[32]; //length
		bufferedChain [2] = new int[32]; //sound





		for (int i=0; i<32; i++) {
			bufferedChain[0][i] = getNextState (noteChain [curState [0]]);
			bufferedChain[1][i] = getNextState (lengthChain [curState [1]]);
			bufferedChain[2][i] = getNextState (soundChain [curState [2]]);
		}

		if (musicStyle == "Techno") {
			for(int i=1; i<4; i++) {
				System.Array.Copy(bufferedChain[0], 0, bufferedChain[0], 8*i, 8);
				System.Array.Copy(bufferedChain[1], 0, bufferedChain[1], 8*i, 8);
				System.Array.Copy(bufferedChain[2], 0, bufferedChain[2], 8*i, 8);
			}
		}

		else if (musicStyle == "HipHop") {
			
			for(int i=1; i<2; i++) {
				System.Array.Copy(bufferedChain[0], 0, bufferedChain[0], 16*i, 16);
				System.Array.Copy(bufferedChain[1], 0, bufferedChain[1], 16*i, 16);
				System.Array.Copy(bufferedChain[2], 0, bufferedChain[2], 16*i, 1);
			}
			
		}

	}


	/*
	 * gets the next state for a line from the markov matrix
	 */
	private int getNextState(float[] node) {

		float r = Random.value;
		float a = 0;

		for (int i=0; i<node.Length; i++) {
			a += node[i];
			if(a>=r)
				return i;
		}

		return 0;
	}


	/*
	 * initializes a markov transition matrix for a music style
	 */
	public void setChains(string t) {

		musicStyle = t;

		if (t == "HipHop") {

			noteChain = new float[12][];
			noteChain [0] = new float[12]  {0.625f,0,0,0,0,1/8f,0,1/8f,0,0,1/8f,0}; //C -
			noteChain [1] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //C#
			noteChain [2] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //D -
			noteChain [3] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //D# -
			noteChain [4] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //E
			noteChain [5] = new float[12]  {3/4f,0,0,0,0,1/8f,0,1/8f,0,0,0,0}; //F -
			noteChain [6] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //F#
			noteChain [7] = new float[12]  {3/4f,0,0,0,0,1/8f,0,1/8f,0,0,0,0}; //G
			noteChain [8] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //G#
			noteChain [9] = new float[12]  {1/2f,0,0,0,0,0,0,0,0,0,0,0}; //A
			noteChain [10] = new float[12] {1/2f,0,0,0,0,0,0,0,0,1/4f,0,0}; //A#
			noteChain [11] = new float[12] {0,0,0,0,0,0,0,0,0,0,0,0}; //H
			
			// Pause, 1/2, 1/4, 1/8, 1/16
			lengthChain = new float[5][];
			lengthChain[0] = new float[5] {0, 0, 0, 0, 0};
			lengthChain[1] = new float[5] {0, 0, 5/7f, 2/7f, 0};
			lengthChain[2] = new float[5] {0, 4/45f, 38/45f, 3/45f, 0};
			lengthChain[3] = new float[5] {0, 4/8f, 2/8f, 2/8f, 0};
			lengthChain[4] = new float[5] {0, 0, 0, 0, 0};
			
			soundChain = new float[4][];
			soundChain[0] = new float[4] {3/4f, 1/4f, 0, 0};
			soundChain[1] = new float[4] {1/4f, 3/4f, 0, 0};
			soundChain[2] = new float[4] {0, 0, 0, 0};
			soundChain[3] = new float[4] {0, 0, 0, 0f};
			
			curState = new int[3] {0,1,0};

		} else if (t == "Jazz") {

			noteChain = new float[12][];
			noteChain [0] = new float[12]  {3/8f,0,0,0,0,2/8f,0,1/8f,0,0,2/8f,0}; //C -
			noteChain [1] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //C#
			noteChain [2] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //D -
			noteChain [3] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //D# -
			noteChain [4] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //E
			noteChain [5] = new float[12]  {1/2f,0,0,0,0,1/4f,0,1/4f,0,0,0,0}; //F -
			noteChain [6] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //F#
			noteChain [7] = new float[12]  {3/4f,0,0,0,0,1/8f,0,1/8f,0,0,0,0}; //G
			noteChain [8] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //G#
			noteChain [9] = new float[12]  {1/2f,0,0,0,0,0,0,0,0,0,0,0}; //A
			noteChain [10] = new float[12] {1/2f,0,0,0,0,0,0,0,0,1/4f,0,0}; //A#
			noteChain [11] = new float[12] {0,0,0,0,0,0,0,0,0,0,0,0}; //H
			
			// Pause, 1/2, 1/4, 1/8, 1/16
			lengthChain = new float[5][];
			lengthChain[0] = new float[5] {2/7f, 0, 3/7f, 2/7f, 0};
			lengthChain[1] = new float[5] {2/7f, 0, 3/7f, 2/7f, 0};
			lengthChain[2] = new float[5] {10/45f, 4/45f, 28/45f, 3/45f, 0};
			lengthChain[3] = new float[5] {2/8f, 2/8f, 2/8f, 2/8f, 0};
			lengthChain[4] = new float[5] {0, 0, 0, 0, 0};
			
			soundChain = new float[4][];
			soundChain[0] = new float[4] {3/4f, 1/4f, 0, 0};
			soundChain[1] = new float[4] {1/4f, 3/4f, 0, 0};
			soundChain[2] = new float[4] {0, 0, 0, 0};
			soundChain[3] = new float[4] {0, 0, 0, 0f};
			
			curState = new int[3] {0,1,0};


		} else {

			noteChain = new float[12][];
			noteChain [0] = new float[12]  {0.625f,0,0,0,0,1/8f,0,1/8f,0,0,1/8f,0}; //C -
			noteChain [1] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //C#
			noteChain [2] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //D -
			noteChain [3] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //D# -
			noteChain [4] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //E
			noteChain [5] = new float[12]  {3/4f,0,0,0,0,1/8f,0,1/8f,0,0,0,0}; //F -
			noteChain [6] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //F#
			noteChain [7] = new float[12]  {3/4f,0,0,0,0,1/8f,0,1/8f,0,0,0,0}; //G
			noteChain [8] = new float[12]  {0,0,0,0,0,0,0,0,0,0,0,0}; //G#
			noteChain [9] = new float[12]  {1/2f,0,0,0,0,0,0,0,0,0,0,0}; //A
			noteChain [10] = new float[12] {1/2f,0,0,0,0,0,0,0,0,1/4f,0,0}; //A#
			noteChain [11] = new float[12] {0,0,0,0,0,0,0,0,0,0,0,0}; //H

			// 1, 1/2, 1/4, 1/8, 1/16
			lengthChain = new float[5][];
			lengthChain[0] = new float[5] {0, 0, 0, 0, 0};
			lengthChain[1] = new float[5] {0, 0, 4/7f, 3/7f, 0};
			lengthChain[2] = new float[5] {0, 3/45f, 40/45f, 2/45f, 0};
			lengthChain[3] = new float[5] {0, 4/8f, 2/8f, 2/8f, 0};
			lengthChain[4] = new float[5] {0, 0, 0, 0, 0};

			soundChain = new float[4][];
			soundChain[0] = new float[4] {1f, 0, 0, 0};
			soundChain[1] = new float[4] {0, 0, 0, 0};
			soundChain[2] = new float[4] {0, 0, 0, 0};
			soundChain[3] = new float[4] {0, 0, 0, 0f};

			curState = new int[3] {0,1,0};

		}
	}

	
}
