
using UnityEngine;
using System;
using System.Collections.Generic;


public class synthSound  : MonoBehaviour
	{

	private string eventtype, soundtype, paramtype;
	private float param;
	//private SfxrSynth synth;

	private static Dictionary<string, SfxrSynth> synthBankCache = new Dictionary<string, SfxrSynth>();
	
	

	private static SfxrSynth[] synthBank;

	public static string[] soundNames = {"Bass", "Bell", "Bleepers", "ChicagoHouse", "Clap", "Flutter", "Guitar", "LowEnd", "Rhythmic", "Spheres"};


	private AudioClip[] audioClips;
	private AudioSource audioSource;

	

	void Start() {
		audioClips = new AudioClip[3];
		audioSource = gameObject.AddComponent<AudioSource>();
	}


	public static string[] getSoundNames() {
		return soundNames;
	}

	public void receiveParam(float p) {
		param = p;
	}

	public void receiveEvent(string s) {
		if(eventtype.Equals(s) && !soundtype.Equals("") && !paramtype.Equals("")) {

			string cachedSoundName;
			float step = ((float)(Math.Truncate((double)param*10.0)));

			if(step==1)
				cachedSoundName = soundtype + "/10";
			else
				cachedSoundName = soundtype + "/0" + step.ToString() + "";


			//"SoundBank/Bass_CMaj/05"

			//audioClips[0] = Resources.Load(cachedSoundName, typeof(AudioClip)) as AudioClip;
			audioClips[0] = (AudioClip) Resources.Load(cachedSoundName) as AudioClip;

			audioSource.PlayOneShot(audioClips[0]);
			Debug.Log("try to play " + cachedSoundName);


		}
	}


	
	// Mapping
	public void setEventtype(string s) {
		eventtype = s;
	}
	
	public void setParamtype(string s) {
		paramtype = s;
	}

	public string getParamType() {
		return paramtype;
	}
	
	public void setSoundtype(string s) {
		soundtype = s;
	}


	}


