﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class MappingService : MonoBehaviour  {

	RegistryService regServ;
	//Dictionary<string, SfxrSynth> synthBank;

	synthSound[] synthBank = new synthSound[5];


	void Start () {
		//synthBank = new Dictionary<string, SfxrSynth>();
		regServ = this.GetComponent<RegistryService>();

		for (int i=0; i<synthBank.Length; i++) {
			//synthBank[i] = new synthSound();

			synthBank[i] = gameObject.AddComponent<synthSound>();
		}

	}


	
	public void receiveEvent(string id) {
		foreach (synthSound element in synthBank) {

			if(element.getParamType()!=null) {

				float param = regServ.getNormalizedValue(element.getParamType());

				element.receiveParam(param);
				element.receiveEvent(id);
			}
		}
	}


	public void addMapping(string t, string s) {
		Debug.Log ("Add mapping for " + t + " - " + s);

		string[] split = t.Split (new char[] {'_'});

		try
		{
			int synthPos = Convert.ToInt32 (split [1]);

			switch (split[0])
			{
				case "param":
					synthBank[synthPos].setParamtype(s);
				break;

				case "event":
					synthBank[synthPos].setEventtype(s);
				break;

				default:
					synthBank[synthPos].setSoundtype(s);
				break;
			}

		}
		catch {
			Debug.LogError("Mapping of " + t + " - " + s + " failed!");
		}


	}



}
	