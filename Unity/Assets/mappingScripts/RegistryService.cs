﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;


public class RegistryService  : MonoBehaviour  {

	private Dictionary<string, float[]> paramRegistry;
	private ArrayList eventRegistry;
	private static int id = 0;
	private MappingService mapping;

	void Start () {
		paramRegistry = new Dictionary<string, float[]>();
		eventRegistry = new ArrayList();
		mapping = this.GetComponent<MappingService> ();
	}


	public void registerParam(string id, float from, float to, float value) {
		paramRegistry.Add(id, new float[] {from, to, value});
	}


	public void receiveParam(string id, float value) {

		//Debug.Log ("Received Param: " + value);
		float[] tmpArray = new float[3];

		if(paramRegistry.TryGetValue(id, out tmpArray)) {	
			tmpArray.SetValue(value, 2);
			paramRegistry[id] = tmpArray;
		}
	}

	public void registerEvent(string id) {
		eventRegistry.Add (id);
	}


	/* Receives an event by a game object and sends a normalized
	 * event-value pair to the mapping service
	 */
	public void receiveEvent(string id) {
		mapping.receiveEvent(id);
	}


	
	public Dictionary<string, float[]> getAllParams() {
		return paramRegistry;
	}

	public ArrayList getEventRegistry() {
		return eventRegistry;
	}

	/**
	 * Returns a normalized [0-1] float value for an existing id
	 */
	public float getNormalizedValue(string id) {

		float[] tmpArray = new float[3];

		if(paramRegistry.TryGetValue(id, out tmpArray)) {
			float normOut;



			normOut =  (tmpArray[2] - tmpArray[0]) / (tmpArray[1] - tmpArray[0]) ;

			if(normOut<=0) {return 0;}
			return normOut;

		} else {
			return 0;
		}
	}

	public int createID() {
		return id++;
	}

}
