﻿using UnityEngine;
using System.Collections;

public class obstacle : MonoBehaviour {

	public GameObject explosion;
	private bool horizontal=true;
	private float rotPerFrame=0f;

	void Start() {
		rotPerFrame = Random.Range (-1F, 1F);
	}

	void Update() {
		transform.Rotate(new Vector3(0,0,rotPerFrame));
	}
	
	void OnTriggerEnter2D(Collider2D collision){
		GameObject level = GameObject.Find("LevelScript");

		if(collision.gameObject.name=="bomb(Clone)")
			Destroy (collision.gameObject);

		Destroy (gameObject);
		Instantiate(explosion, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);

	}

	public void changeOrientation() {
		horizontal=!horizontal;	
	}

	public void setAsHorizontal(bool h) {
		if(h) {
			transform.eulerAngles = new Vector3(0, 0, 90);
		}
		horizontal=h;
	}

}
