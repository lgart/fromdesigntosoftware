﻿using UnityEngine;
using System.Collections;

public class enemyKI : MonoBehaviour {

	private static float startingDirection = -1;

	private int moveCounter=0, bombCounter;
	private const float BORDER_X=6f, BORDER_Y_TOP=2.8f, BORDER_Y_BOTTOM=-4f;
	private bool horizontal=true;
	private Vector3 startPos, endPos, startRot, endRot; //From/To for animation

	private float startTime;

	public GameObject level;
	public Transform bomb; 


	private RegistryService regServ;
	private string id, idY, idEvent;

	private float speed = 2f;

	void Start () {
		bombCounter=Random.Range(50, 350);

		GetComponent<Rigidbody2D>().velocity = new Vector2(speed*startingDirection, 0);
		startingDirection = startingDirection*-1;

		startPos=new Vector3(0,0,1);

	}



	void Update () {

		//Register an ID / Event
		if(idEvent==null) {
			regServ = level.GetComponent<RegistryService>();

			idEvent = "Bomb [" + regServ.createID().ToString() + "]";
			regServ.registerEvent(idEvent);

		}



		//Change to other axis
		if(level.GetComponent<level>().isHorizontal() != horizontal) {
			animatedMove();
		}

		else {
			//Move horizontal
			if(horizontal) 
			{
				if( (transform.position.x > BORDER_X) || (transform.position.x < -BORDER_X) ) {
					changeDirection();
				}
			//Move vertical
			} else {
				if( (transform.position.y > BORDER_Y_TOP) || (transform.position.y < BORDER_Y_BOTTOM) ) {
					changeDirection();
				}
			}
		}
	}


	public void dropBomb() {
		Instantiate(bomb, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);

		regServ.receiveEvent (idEvent);


	}



	public void changeDirection() {

		if(horizontal) {
			if(GetComponent<Rigidbody2D>().velocity.x > 0) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, 0);
			} else {
				GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
			}
		} else {
			if(GetComponent<Rigidbody2D>().velocity.y > 0) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);
			} else {
				GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
			}
		}

	}

	public void setSpeed(float s) {
		speed = (s-0.3f)*2f;
		Debug.Log ("New Speed Set " + speed);
	}

	void animatedMove() {


		if(startPos.z==1) {
			startPos = transform.position;
			startTime = Time.time;

			//startRot = new Vector3(0, 0, 0);
			endRot = new Vector3(0, 0, 270);

			GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
			moveCounter=0;

			if(horizontal) {
				if(transform.position.y>0) {
					endPos=new Vector3(-5.9f, 0, 0);
				} else {
					endPos=new Vector3(5.9f, 0, 0);
					endRot = new Vector3(0, 0, 90);
				}
			} else {
				if(transform.position.x>0) {
					endPos=new Vector3(0, 4f, 0);
					endRot = new Vector3(0, 0, 180);
				} else {
					endPos=new Vector3(0, -4f, 0);
					endRot = new Vector3(0, 0, 0);
				}
			}

			//transform.Rotate(new Vector3(0, 0, 90));
		}

		transform.position = Vector3.Slerp(startPos, endPos, (Time.time - startTime));
		
		transform.Rotate(new Vector3(0,0,10));

		if((Time.time - startTime) >= 1) {
			startPos=new Vector3(0,0,1);
			horizontal = level.GetComponent<level>().isHorizontal();
			transform.eulerAngles = endRot;
			Debug.Log("-- - - Finished Moving ");

			if(horizontal) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(speed*startingDirection, 0);
				startingDirection = startingDirection*-1;
			} else  {
				GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed*startingDirection);
				startingDirection = startingDirection*-1;
			}
		}

	}
}
