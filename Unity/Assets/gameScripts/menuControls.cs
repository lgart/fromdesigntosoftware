﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;


public class menuControls : MonoBehaviour {

	public Transform eventBtn;
	public RegistryService regServ;
	public MappingService mapServ;
	
	private bool createEventPrefab=true;	
	private bool createParamPrefab=true;
	private bool createSoundPrefab=true;

	private IList<GameObject> panels;

	void Start() {
		panels = new List<GameObject> ();
	}

	public void createEventPrefabs() {

		if (createEventPrefab) {
			GameObject g = null;
			GameObject[] pAllObjects = (GameObject[])Resources.FindObjectsOfTypeAll (typeof(GameObject));
			ArrayList eventList = regServ.getEventRegistry ();


			foreach (GameObject pObject in pAllObjects) {
				if (pObject.name == "EventSelectorPanel") {

					g = pObject;
					panels.Add(g);

					string pos = g.transform.parent.name;
					pos = pos.Substring(13,1);

					int j = 0;
					
					foreach (string s in eventList) {

						Transform button1 = Instantiate (eventBtn);
						
						button1.SetParent (g.transform);


						RectTransform rectBtn1 = (RectTransform)button1.GetComponent<RectTransform> ();
						rectBtn1.anchoredPosition = new Vector2 (0, -24 * (j++));
						
						Text t = (button1.GetChild (0)).GetComponent<Text> ();
						t.text = s;
						
						Button b = button1.GetComponent<Button> ();

						string capture1 = "event_" + pos;
						string capture2 = s;
						Transform capture3 = g.transform;


						b.onClick.AddListener (() => {
							this.buttonListener (capture1, capture2, capture3); });


					}
					j++;
				}
			}

			createEventPrefab=false;
		}

	}

	public void createParamPrefabs() {

		if (createParamPrefab) {

			GameObject g = null;
			GameObject[] pAllObjects = (GameObject[])Resources.FindObjectsOfTypeAll (typeof(GameObject));
			Dictionary<string, float[]> eventList = regServ.getAllParams ();
		
			int i = 0;
			foreach (GameObject pObject in pAllObjects) {
				if (pObject.name == "ParamSelectorPanel") {
					g = pObject;
					panels.Add(g);
				
					int j = 0;

					string pos = g.transform.parent.name;
					pos = pos.Substring(13,1);

					foreach (KeyValuePair<string, float[]> entry in eventList) {
						
						Transform button1 = Instantiate (eventBtn);
						button1.SetParent (g.transform);
						
						RectTransform rectBtn1 = (RectTransform)button1.GetComponent<RectTransform> ();
						rectBtn1.anchoredPosition = new Vector2 (0, -24 * (j++));
						
						Text t = (button1.GetChild (0)).GetComponent<Text> ();
						t.text = entry.Key;
						
						Button b = button1.GetComponent<Button> ();

						string capture1 = "param_" + pos;
						string capture2 = entry.Key;
						Transform capture3 = g.transform;
						b.onClick.AddListener (() => {
							this.buttonListener (capture1, capture2, capture3); });

					}
					i++;
				}
			}
			createParamPrefab=false;
		}
	}


	public void createSoundPrefabs() {
		
		if (createSoundPrefab) {
			
			GameObject g = null;
			GameObject[] pAllObjects = (GameObject[])Resources.FindObjectsOfTypeAll (typeof(GameObject));
			Dictionary<string, float[]> eventList = regServ.getAllParams ();

			int i = 0;
			foreach (GameObject pObject in pAllObjects) {
				if (pObject.name == "SoundSelectorPanel") {
					g = pObject;
					panels.Add(g);

					int j = 0;

					string pos = g.transform.parent.name;
					pos = pos.Substring(13,1);

					foreach (string s in synthSound.getSoundNames()) {
						
						Transform button1 = Instantiate (eventBtn);
						button1.SetParent (g.transform);
						
						RectTransform rectBtn1 = (RectTransform)button1.GetComponent<RectTransform> ();
						rectBtn1.anchoredPosition = new Vector2 (0, -24 * (j++));
						
						Text t = (button1.GetChild (0)).GetComponent<Text> ();
						t.text = s;
						
						Button b = button1.GetComponent<Button> ();

						string capture1 = "sound_" + pos;
						string capture2 = s;
						Transform capture3 = g.transform;
						b.onClick.AddListener (() => {
							this.buttonListener (capture1, capture2, capture3); });
						
						
					}
					i++;
				}
			}

			createSoundPrefab=false;
		}
	}


	public void randomizeMapping() {

		Debug.Log(" - Try to get all panel values");

		/*
		IEnumerator e = panels.GetEnumerator;

		while(e.MoveNext) {
			((GameObject) e.Current).
		}
*/

		int i=0, r=0;

		foreach(GameObject g in panels)
		{

			r=Random.Range(0, 5);
			if(g.name == "SoundSelectorPanel") 
				r=Random.Range(0, 8);
			i=0;

			foreach(Transform c in g.transform) {

				if((i++)==r) {
					c.GetComponent<Button>().onClick.Invoke();
					break;
				}
	
			}

		}

	}

	public void hideAllPanels() {
		foreach (GameObject g in panels) {
			g.SetActive(false);
		}
	}

	public void buttonListener(string t, string s, Transform p) {
		
		Text te = (p.parent.GetChild(0)).GetComponent<Text>();
		te.text = s;

		p.gameObject.SetActive (false);

		mapServ.addMapping(t,s);

	}

}
