﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class timeControl : MonoBehaviour {

	public int beatPosition=0;
	public GUIText GuiBPM;

	public GameObject enemyTop, enemyBottom, level, GuiBPMMenu, buttonBPM, buttonRhythm, buttonDynSound;
	public GameObject bg1, bg2, bg3, bg4;
	private markovChains markovChain;

	private string rhythm = "";

	private bool[] enemyTopShot;
	private bool[] enemyBottomShot;
	private bool[] enemyMove;	
	private bool[] obstacleRotation;

	private bool bgMusicOn=true, bgBeatOn=true;

	
	private AudioClip[] audioClips;
	private AudioSource audioSource;
	private int[] randomPercRhythm;

	private float starOpacity;
	Color tempcolor;


	void Start () {

		setBPM(120);
		randomPercRhythm = new int[64];


		markovChain = gameObject.AddComponent<markovChains> ();
		setRhythm("Techno");

		audioSource = gameObject.AddComponent<AudioSource>();
		audioClips = new AudioClip[4];
		audioClips[0] = (AudioClip) Resources.Load("simple_beat/kick") as AudioClip;
		audioClips[1] = (AudioClip) Resources.Load("simple_beat/snare") as AudioClip;
		audioClips[2] = (AudioClip) Resources.Load("simple_beat/shrad") as AudioClip;
		audioClips[3] = (AudioClip) Resources.Load("simple_beat/stazza") as AudioClip;


	}
	
	/* triggers beat dependent actions */
	void beatActions() {
		beatPosition = (beatPosition+1)%64;


		if(beatPosition%16 < 8) {
			starOpacity = ((beatPosition%8)/8f);
		} else {
			starOpacity = 1-((beatPosition%8)/8f);
		}

		tempcolor = bg4.GetComponent<Renderer>().material.color;
		tempcolor.a = starOpacity;
		bg4.GetComponent<Renderer>().material.color = tempcolor;

		tempcolor = bg3.GetComponent<Renderer>().material.color;
		tempcolor.a = 1f-starOpacity;
		bg3.GetComponent<Renderer>().material.color = tempcolor;

		tempcolor = bg1.GetComponent<Renderer>().material.color;
		tempcolor.a = 1-((beatPosition%8)/16f);
		bg1.GetComponent<Renderer>().material.color = tempcolor;


		if(enemyTopShot[beatPosition]) {  enemyTop.SendMessage("dropBomb"); }
		if(enemyBottomShot[beatPosition]) {  enemyBottom.SendMessage("dropBomb"); }
		if(enemyMove[beatPosition]) {  level.GetComponent<level>().invertHorizontal(); }
		if(obstacleRotation[beatPosition]) { level.GetComponent<level>().rotateObstacles(); }

		if (rhythm == "HipHop") {

			if (bgMusicOn) {
				if (beatPosition % 12 == 0) {
					markovChain.transition ();
				}
				if (beatPosition % 16 == 0) {
					markovChain.transition ();
				}
			}

			if (bgBeatOn) {
				if (beatPosition % 12 == 0) {
					audioSource.PlayOneShot (audioClips [1]);
				}
				if (beatPosition % 8 == 0) {
					audioSource.PlayOneShot (audioClips [0]);
				}
				if (beatPosition % 18 == 0) {
					audioSource.PlayOneShot (audioClips [0]);
				}
			}


		} else if (rhythm == "Jazz") {

			if (bgMusicOn) {
				if (beatPosition % 2 == 0 && Random.value > 0.5f) 
					markovChain.transition ();

				if (beatPosition % 3 == 0 && Random.value > 0.9f) 
					markovChain.transition ();
			}

			if (bgBeatOn) {
				if (beatPosition % 6 == 0) {
					audioSource.PlayOneShot (audioClips [0]);
				}
			}


		} else {

			if (bgMusicOn) {
				if (beatPosition % 16 == 0) {
					markovChain.transition ();
				}
			}

			if (bgBeatOn) {
				if (beatPosition % 16 == 0) {
					audioSource.PlayOneShot (audioClips [1]);
				}
			

				if (beatPosition % 4 == 0) {
					audioSource.PlayOneShot (audioClips [0]);
				}
		
			}
		}

		if (bgBeatOn) {
			if (randomPercRhythm [beatPosition] == 1)
				audioSource.PlayOneShot (audioClips [1]);
			else if (randomPercRhythm [beatPosition] == 2)
				audioSource.PlayOneShot (audioClips [2]);
		}

	}



	public void setBPM(float beatPerMinute) {
		CancelInvoke("beatActions");

		float t = (60/beatPerMinute)/4; // BPM / Seconds / Beats
		InvokeRepeating("beatActions", 0, t);
		GuiBPM.text = beatPerMinute + " BPM = " + t + " sec";

		enemyTop.SendMessage ("setSpeed", beatPerMinute / 60);
		enemyBottom.SendMessage ("setSpeed", beatPerMinute / 60);


		Text te = (buttonBPM.transform.GetChild(0)).GetComponent<Text>();
		te.text = beatPerMinute + " BPM";

	}


	/**
	 * generates a random percussive rhythm
	 * 1 / 2 -> drumhit 1 / 2
	 * 0 -> no drumhit
	 * for techno & hiphop pattern gets shortened
	 */
	public void genRandomPercRhythm() {

		Debug.Log("New rhythm created");
		for (int i=0; i<randomPercRhythm.Length; i++) {

			float r = Random.value;

			if(r >0.8f && r <0.9f)
				randomPercRhythm[i]=1;
			else if(r >0.9f)
				randomPercRhythm[i]=2;
			else
				randomPercRhythm[i]=0;
		}

		// repeat pattern 4 times / 8 times for hiphop / techno
		if(rhythm=="HipHop") {
			for(int i=1; i<4; i++) {
				System.Array.Copy(randomPercRhythm, 0, randomPercRhythm, 16*i, 16);
			}
		} else if(rhythm=="Techno") {
			for(int i=1; i<8; i++) {
				System.Array.Copy(randomPercRhythm, 0, randomPercRhythm, 8*i, 8);
			}
		}


	}

	/* set music / beat on or off */
	public void setBackgroundMusic(string s) {
		Text te = (buttonDynSound.transform.GetChild(0)).GetComponent<Text>();
		te.text = s;

		switch (s) {
			case "Off":
				bgMusicOn=false;
				bgBeatOn=false;
				break;
			case "Beat":
				bgMusicOn=false;
				bgBeatOn=true;
				break;
			case "Melody":
				bgMusicOn=true;
				bgBeatOn=false;
				break;
			default:
				bgMusicOn=true;
				bgBeatOn=true;
			break;
		}
	}

	/* rhythmic behaviour of game elements */
	public void setRhythm(string r) {

		Text te = (buttonRhythm.transform.GetChild(0)).GetComponent<Text>();
		te.text = r;

		rhythm = r;

		markovChain.setChains (r);
		genRandomPercRhythm ();

		if (r == "HipHop") {
			enemyMove = new bool[64] {
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false};

			enemyTopShot = new bool[64] {
				true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};

			enemyBottomShot = new bool[64] {
				false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};

			obstacleRotation = new bool[64] {
				false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false,
				false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false};


		} else if (r == "Jazz") {

			enemyTopShot = new bool[64] {
				false, false, false, false, true, true, true, false, false, false, false, false, false, false, false, false,
				false, false, true, false, false, false, false, true, false, false, false, false, false, false, true, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false};

			enemyBottomShot = new bool[64] {
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true,
				false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false};

			enemyMove = new bool[64] {
				false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false,
				false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};

			obstacleRotation = new bool[64] {
				false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false,
				true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false,
				false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false,
				false, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false};
		} else {
			enemyTopShot = new bool[64] {
				true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
			enemyBottomShot = new bool[64] {
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
			enemyMove = new bool[64] {
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false};
			obstacleRotation = new bool[64] {
				false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
		}
	}

}
