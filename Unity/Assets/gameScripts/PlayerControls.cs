﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {
	
	
	public KeyCode moveUp, moveDown, moveLeft, moveRight, space;
	
	private Vector2 speed;
	private float speedLimit=2.3f;
	private int turbo = 1, maxSpeed=3;
	private int flameCounter=0;
	private float dontDieTwiceTimer;
	private bool spriteActive=true;
	
	public GameObject level, background1, background2, background3, background4, explosion, flareFlash, flareFire, flareStream, fingerPointer;

	private Component levelScript;
	private float shipDirection;
	
	private const float BORDER_Y=3f, BORDER_X=6f;
	
	public GUIText guiPoints, guiLives;
	private int points=0, lives=5;
	private string shipXID, shipYID, shipDestroyedEvent, phoneAccXID, phoneAccYID, randomValID;
	private RegistryService regServ;


	Vector3 posDiff = new Vector3(0, 0, 0);
	float rotShip = 0;
	
	void Start () {
		speed = new Vector2(0, 0);

		background1 = GameObject.Find("background_1");
		background2 = GameObject.Find("background_2");
		background3 = GameObject.Find("background_3");
		background4 = GameObject.Find("background_4");

		dontDieTwiceTimer=Time.time-2;
		levelScript = level.GetComponent<level>();
	}
	
	
	
	void Update () {


		if(shipXID==null) {
			regServ = level.GetComponent<RegistryService>();

			shipXID = "Ship x-Pos [" + regServ.createID().ToString() + "]";
			regServ.registerParam(shipXID, -BORDER_X, BORDER_X, transform.position.x);

			shipYID = "Ship y-Pos [" + regServ.createID().ToString() + "]";
			regServ.registerParam(shipYID, -BORDER_Y, BORDER_Y, transform.position.y);

			phoneAccXID = "Phone x-Pos [" + regServ.createID().ToString() + "]";
			regServ.registerParam(phoneAccXID, 0, 1, (Input.acceleration.x+1)/2);

			phoneAccYID = "Phone y-Pos [" + regServ.createID().ToString() + "]";
			regServ.registerParam(phoneAccYID, 0, 1, (Input.acceleration.y+1)/2);

			randomValID = "Random [" + regServ.createID().ToString() + "]";
			regServ.registerParam(randomValID, 0, 1, Random.Range(0F, 1F));


			shipDestroyedEvent = "Ship destroyed [" + regServ.createID().ToString() + "]";
			regServ.registerEvent(shipDestroyedEvent);
			
		}


		regServ.receiveParam(shipXID, transform.position.x);
		regServ.receiveParam(shipYID, transform.position.y);
		regServ.receiveParam(phoneAccXID, (Input.acceleration.x+1)/2);
		regServ.receiveParam(phoneAccYID, (Input.acceleration.y+1)/2);
		regServ.receiveParam(randomValID, Random.Range(0F, 1F));



		if(spriteActive && !((level)levelScript).isPaused()) {

		background1.transform.position = new Vector3((-0.15f)*transform.position.x, (-0.15f)*transform.position.y, 0);
		background2.transform.position = new Vector3((-0.1f)*transform.position.x, (-0.1f)*transform.position.y, 0);
		background3.transform.position = new Vector3((0.1f)*transform.position.x, (0.1f)*transform.position.y, 0);
		background4.transform.position = new Vector3((-0.2f)*transform.position.x, (-0.2f)*transform.position.y, 0);
		
		// Show flames
		if((flameCounter--)>0)  {
			flareFlash.SetActive(true);
			flareFire.SetActive(true);
		} else {
			flareFlash.SetActive(false);
			flareFire.SetActive(false);
		}

		
		


		// TOUCH Controls
		if (Input.touchCount > 0) {
			
			bool pointerActive=true;
			Touch touch=Input.GetTouch(0);
			
			for (int i = 0; i < Input.touchCount; i++) {
				
				touch = Input.GetTouch(i);
				
				if(touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
					pointerActive=false;
				}
				
			}
			
			
			Vector3 fingerPos = Camera.main.ScreenToWorldPoint(touch.position);
			fingerPointer.transform.position = new Vector3(fingerPos.x, fingerPos.y, 0);
			fingerPointer.SetActive(pointerActive);
			flameCounter=10;

			//Debug.Log("POS X: " + fingerPos.x +  " - POS Y: " +  fingerPos.y);


			// [+X / +Y]
			if(fingerPos.x > transform.position.x && fingerPos.y > transform.position.y) {
					posDiff.x = fingerPos.x - transform.position.x;
					posDiff.y = fingerPos.y - transform.position.y;

					rotShip = Mathf.Atan(posDiff.y/posDiff.x) * (180/Mathf.PI);
					rotShip += 270;


			}
			// [+X / -Y]
			else if (fingerPos.x > transform.position.x && fingerPos.y < transform.position.y) {
					posDiff.x = fingerPos.x - transform.position.x;
					posDiff.y = transform.position.y - fingerPos.y;

					rotShip = Mathf.Atan(posDiff.x/posDiff.y) * (180/Mathf.PI);
					rotShip += 180;
				

			}
			// [-X / -Y]
			else if (fingerPos.x < transform.position.x && fingerPos.y < transform.position.y) {
					posDiff.x = transform.position.x - fingerPos.x;
					posDiff.y = transform.position.y - fingerPos.y;

					rotShip = Mathf.Atan(posDiff.y/posDiff.x) * (180/Mathf.PI);
					rotShip += 90;
			}
			// [-X / +Y]
			else{
					posDiff.x = transform.position.x - fingerPos.x;
					posDiff.y = fingerPos.y - transform.position.y;

					rotShip = Mathf.Atan(posDiff.x/posDiff.y) * (180/Mathf.PI);

			}

			/*
			 * Depending on the angle interval (90/180/270/360) the x/y values for speed 
			 * change are either positive or negative float 
			 */
			if(rotShip<90) {
				speed.x = -1* ((rotShip%90))/90;
				speed.y = ((90-rotShip)%90)/90;
			} else if(rotShip<180) {
				speed.x = -1*(90-(rotShip%90))/90;
				speed.y = -1*(rotShip%90)/90;
			} else if(rotShip<270) {
				speed.x = (rotShip%90)/90;
				speed.y = -1*(90-(rotShip%90))/90;
			} else {
				speed.x = (90-(rotShip%90))/90;
				speed.y = (rotShip%90)/90;
			}
			
				speed = speed*4;


				transform.localEulerAngles = new Vector3(0, 0, rotShip);


			
		} else {
			fingerPointer.SetActive(false);
			speed.x=0;
			speed.y=0;
		}


		
		
		
		// KEY Controls
		if((Input.GetKey(moveUp)) && (speed.y < maxSpeed*turbo)) {
			speed.y+=2*turbo;
			transform.localEulerAngles = new Vector3(0, 0, 0);
			
			flameCounter=10;
		}
		if((Input.GetKey(moveDown)) && (speed.y > -maxSpeed*turbo)) {
			speed.y-=2*turbo;
			transform.localEulerAngles = new Vector3(0, 0, 180);
			
			flameCounter=10;
		}
		if((Input.GetKey(moveRight)) && (speed.x < maxSpeed*turbo)) {
			speed.x+=2*turbo;
			transform.localEulerAngles = new Vector3(0, 0, 270);
			
			flameCounter=10;
		}
		if((Input.GetKey(moveLeft)) && (speed.x > -maxSpeed*turbo)) {
			speed.x-=2*turbo;
			transform.localEulerAngles = new Vector3(0, 0, 90);
			
			flameCounter=10;
		}
		
		
		// Check edge-collision
		if(transform.position.y > BORDER_Y) {
			transform.position = new Vector3(transform.position.x, BORDER_Y, transform.position.z);
		} else if(transform.position.y < -BORDER_Y) {
			transform.position = new Vector3(transform.position.x, -BORDER_Y, transform.position.z);
		}
		
		if(transform.position.x > BORDER_X) {
			transform.position = new Vector3(BORDER_X, transform.position.y, transform.position.z);
		} else if(transform.position.x < -BORDER_X) {
			transform.position = new Vector3(-BORDER_X, transform.position.y, transform.position.z);
		}
		
		// Move the ship
		GetComponent<Rigidbody2D>().velocity = speed;
		}
	}
	
	void OnTriggerEnter2D(Collider2D collision){
		Debug.Log("-- Collision 2");
		
		if(collision.gameObject.tag.Equals("brick")) {
			Debug.Log("-- Brick collected");
			points+=100;
			guiPoints.text=points.ToString();
			Destroy(collision.gameObject);
			
			/*
			GameObject[] gos;
			gos = GameObject.FindGameObjectsWithTag("brick");

			if(gos.Length==0) {
				guiPoints.text="Level Completed";
			}
			*/
			level.GetComponent<level>().removeBrick();
			
			
		}


		if(collision.gameObject.tag.Equals("bomb") || collision.gameObject.tag.Equals("obstacle")) {



			if((Time.time-dontDieTwiceTimer)>2) {

				regServ.receiveEvent (shipDestroyedEvent);

				dontDieTwiceTimer=Time.time;
				
				Debug.Log("-- Ship destroyed");
				
				Instantiate(explosion, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
				
				lives--;
				guiLives.text=lives.ToString() + " Lives";
				transform.GetComponent<SpriteRenderer>().enabled=false;
				flareStream.SetActive(false);
				flareFire.SetActive(false);
				flareFlash.SetActive(false);
				spriteActive=false;
				flameCounter=0;
				
				Invoke("died", 2);
			}



		}

	}
	
	void died() {

		if(lives>1)
			level.GetComponent<level>().setPauseCounter(50, lives.ToString() + " lives left!");
		else if (lives==1) {
			level.GetComponent<level>().setPauseCounter(50, lives.ToString() + " life left!");
		} else {
			level.GetComponent<level>().setGameOver();
		}
		
		transform.GetComponent<SpriteRenderer>().enabled=true;
		flareStream.SetActive(true);
		flareFire.SetActive(true);
		flareFlash.SetActive(true);
		spriteActive=true;
		transform.position = new Vector3(0, 0, 0);
		
		// Remove all bombs after collision to prevent death on respawn
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("bomb");
		foreach (GameObject go in gos) {
			Destroy(go);
		}
		
	}

	public void setLives(int l) {
		lives = l;
		guiLives.text=lives.ToString() + " Lives";
	}

	public void dontDieRightNow(float seconds) {
		dontDieTwiceTimer=Time.time-seconds;
	}


	
}
