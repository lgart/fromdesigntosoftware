﻿using UnityEngine;


// Zero-G Sine
using System.Collections.Generic;

public class level : MonoBehaviour {


	public Transform brick;

	public GameObject obstacle, ship, enemyTop, enemyBottom, timeControl; // Game Elements
	public GameObject panel, settingsButton, settingsButtonStart, helpButton, startButton, startScreenGui, startScreenBG, helpScreen, closeHelp; // Menu Elements
	public bool pause=true, gameOver=false;
	public KeyCode space, moveEnemies, moveObstacles;
	public bool swipeLeft, swipeRight;
	public GUIText status;

	private bool showMenu=false;


	private bool horizontal=true;
	private int curLevel=0;
	private int brickCount=0;
	private int pauseCounter=100000000;
	private int rotationCounter=0;
	private const float BORDER_Y=2.8f, BORDER_X=5f;
	private float changeEnemyPos, changeObstaclePos;

	private bool startScreen=true, showHelp=false;


	private RegistryService regServ;
	private string obsRotation, enemyRotation;

	private AudioClip audioClip;
	private AudioSource audioSource;
	private float bgMusicTimer=0;

	public AudioSource bgMusic;

	void Start () {
		buildLevel(curLevel);
		buildObstacle(curLevel);
		changeEnemyPos=Time.time;	
		changeObstaclePos=Time.time;

		//audioSource = gameObject.AddComponent<AudioSource>();
		//audioClip = (AudioClip) Resources.Load("menu") as AudioClip;

		//audioSource.loop = true;
		//audioSource.PlayOneShot (audioClip);
	}
	

	void Update () {

		swipeLeft=false;
		swipeRight=false;

		//Register an ID / Event
		if(regServ==null) {
			regServ = GetComponent<RegistryService>();
			
			obsRotation = "Obstacle rotation [" + regServ.createID().ToString() + "]";
			regServ.registerEvent(obsRotation);

			enemyRotation = "Enemy rotation [" + regServ.createID().ToString() + "]";
			regServ.registerEvent(enemyRotation);
			
		}

		// Touch Input
		if (Input.touchCount > 0) {

			for (int i = 0; i < Input.touchCount; i++) {

				Touch touch=Input.GetTouch(i);

				if(touch.phase == TouchPhase.Ended) {
					touch = Input.GetTouch(i);

					Vector3 fingerPos = Camera.main.ScreenToWorldPoint(touch.position);
				}
			}
		}




		if(gameOver) {
			Time.timeScale = 0;

		} else {

			if(pauseCounter>0 || startScreen) {
				pause=true;
				pauseCounter--;
				Time.timeScale = 0;


			}
			else {
				pause=false;
				Time.timeScale = 1;
				status.text = "";

				/*
				if((Input.GetKey(moveEnemies)||swipeRight) && ((Time.time-changeEnemyPos)>0.6f) ) {
					horizontal=!horizontal;
					changeEnemyPos=Time.time;
				}

*/
				if((Input.GetKey(moveObstacles)||swipeLeft) && ((Time.time-changeObstaclePos)>0.6f) ) {
					rotateObstacles();
				}


				if(rotationCounter>0) {

					if((Time.time - changeObstaclePos) >= 0.01f) {
						rotationCounter--;
						GameObject o = GameObject.Find("Obstacles");
						changeObstaclePos=Time.time;
						o.transform.Rotate(new Vector3(0,0,1));
					}
				}
			}


			if((brickCount==0) && (pauseCounter==0)) {
				status.text = "L e v e l     c o m p l e t e d";
				pauseCounter=120;

				ship.transform.position =  new Vector3(0, 0, 0);
				ship.transform.localEulerAngles = new Vector3(0, 0, 0);

				horizontal=true;

				buildLevel(++curLevel);
				buildObstacle(curLevel);
			}

		}

	}

	public void startGame() {
		startScreen=false;
		pauseCounter=0;
		bgMusic.Pause();

		settingsButtonStart.SetActive(false);
		startButton.SetActive(false);
		helpButton.SetActive(false);
		startScreenGui.SetActive(false);
		settingsButton.SetActive(true);
		startScreenBG.SetActive(false);

		if(gameOver) {
			Time.timeScale = 1;
			
			// remove all bricks
			GameObject[] gos;
			gos = GameObject.FindGameObjectsWithTag("brick");
			foreach (GameObject go in gos) {
				Destroy(go);
			}
			
			ship.GetComponent<PlayerControls>().setLives(5);
			
			brickCount=0;
			curLevel=0;
			
			buildLevel(0);
			buildObstacle(0);
			changeEnemyPos=Time.time;	
			changeObstaclePos=Time.time;

			gameOver=false;
		}
	}




	public void showHideHelp() {

		showHelp=!showHelp;

		settingsButtonStart.SetActive(!showHelp);
		startButton.SetActive(!showHelp);
		helpButton.SetActive(!showHelp);
		startScreenGui.SetActive(!showHelp);
		startScreenBG.SetActive(!showHelp);
		helpScreen.SetActive(showHelp);
		closeHelp.SetActive(showHelp);
	}



	public void rotateObstacles() {
		if(rotationCounter==0) {

			regServ.receiveEvent (obsRotation);

			changeObstaclePos=Time.time;
			rotationCounter=90;
			
			GameObject[] gos;
			gos = GameObject.FindGameObjectsWithTag("obstacle");
			foreach (GameObject go in gos) {
				go.GetComponent<obstacle>().changeOrientation();
			}
		}
	}

	public void showHideMenu() {

		showMenu=!showMenu;

		panel.SetActive(showMenu);

		if(showMenu) {
			pauseCounter=100000000;

			if(startScreen) {
				settingsButtonStart.SetActive(false);
				startButton.SetActive(false);
				helpButton.SetActive(false);
				startScreenGui.SetActive(false);
				settingsButton.SetActive(true);
			} else {
				bgMusic.Play();
			}
		}

		else {
			pauseCounter=0;

			if(startScreen) {
				settingsButtonStart.SetActive(true);
				startButton.SetActive(true);
				helpButton.SetActive(true);
				startScreenGui.SetActive(true);
				settingsButton.SetActive(false);
			} else {
				bgMusic.Pause();
			}
		}



	}

	
	public void removeBrick() {
		brickCount--;
	}

	public void setPauseCounter(int c, string s) {
		status.text = s;
		pauseCounter=c;
	}

	public void setGameOver() {
		status.text = "G a m e   o v e r";
		gameOver=true;
		startScreen=true;

		showHideMenu();
		showHideMenu();

		bgMusic.Play();
	}
	
	public bool isPaused() {
		return pause;
	}

	public bool isHorizontal() {
		return horizontal;
	}

	public void setHorizontal(bool h) {
		horizontal = h;
	}

	public void invertHorizontal() {
		horizontal = !horizontal;
		regServ.receiveEvent (enemyRotation);
	}


	void buildLevel(int level) {

		timeControl.GetComponent<timeControl>().genRandomPercRhythm();
		ship.GetComponent<PlayerControls>().dontDieRightNow(3);


		Vector2 offset = new Vector2(0.7f, 0.7f); // distance between bricks
		rotationCounter=0;

		int[][] bricks = new int[9][];

		switch (level%10) {

		case 0:
			bricks[0] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[2] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[4] = new int[9] {1,1,1, 0,0,0, 1,1,1};
			bricks[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[6] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[7] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 1:
			bricks[0] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[1] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[2] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[4] = new int[9] {1,1,1, 0,0,0, 1,1,1};
			bricks[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[6] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[7] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[8] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			break;
		
		case 2:
			bricks[0] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[2] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[4] = new int[9] {1,1,1, 0,0,0, 1,1,1};
			bricks[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[6] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[7] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 3:
			bricks[0] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[2] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[4] = new int[9] {1,1,1, 0,0,0, 1,1,1};
			bricks[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[6] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[7] = new int[9] {0,0,0, 0,0,0, 1,1,1};
			bricks[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 4:
			bricks[0] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[1] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[2] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[4] = new int[9] {1,1,1, 0,0,0, 1,1,1};
			bricks[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[6] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[7] = new int[9] {1,1,1, 0,0,0, 0,0,0};
			bricks[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 5:
			bricks[0] = new int[9] {1,0,1, 0,0,0, 1,0,1};
			bricks[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[2] = new int[9] {1,0,1, 0,0,0, 1,0,1};
			bricks[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[4] = new int[9] {1,0,1, 0,0,0, 1,0,1};
			bricks[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[6] = new int[9] {1,0,1, 0,0,0, 1,0,1};
			bricks[7] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[8] = new int[9] {1,0,1, 0,0,0, 1,0,1};
			break;

		case 6:
			bricks[0] = new int[9] {1,0,0, 0,1,0, 0,0,1};
			bricks[1] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[2] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[4] = new int[9] {1,1,1, 0,0,0, 1,1,1};
			bricks[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[6] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[7] = new int[9] {0,0,0, 0,1,0, 0,0,0};
			bricks[8] = new int[9] {1,0,0, 0,1,0, 0,0,1};
			break;

		case 7:
			bricks[0] = new int[9] {1,0,0, 0,0,0, 0,0,1};
			bricks[1] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			bricks[2] = new int[9] {0,0,1, 0,0,0, 1,0,0};
			bricks[3] = new int[9] {0,0,0, 1,0,1, 0,0,0};
			bricks[4] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[5] = new int[9] {0,0,0, 1,0,1, 0,0,0};
			bricks[6] = new int[9] {0,0,1, 0,0,0, 1,0,0};
			bricks[7] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			bricks[8] = new int[9] {1,0,0, 0,0,0, 0,0,1};
			break;

		case 8:
			bricks[0] = new int[9] {1,0,0, 0,1,0, 0,0,1};
			bricks[1] = new int[9] {0,1,0, 0,1,0, 0,1,0};
			bricks[2] = new int[9] {0,0,1, 0,1,0, 1,0,0};
			bricks[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[4] = new int[9] {1,1,1, 0,0,0, 1,1,1};
			bricks[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			bricks[6] = new int[9] {0,0,1, 0,1,0, 1,0,0};
			bricks[7] = new int[9] {0,1,0, 0,1,0, 0,1,0};
			bricks[8] = new int[9] {1,0,0, 0,1,0, 0,0,1};
			break;

		case 9:
			bricks[0] = new int[9] {1,0,1, 0,1,0, 1,0,1};
			bricks[1] = new int[9] {0,1,0, 0,1,0, 0,1,0};
			bricks[2] = new int[9] {0,0,1, 0,1,0, 1,0,0};
			bricks[3] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			bricks[4] = new int[9] {1,0,1, 0,0,0, 1,0,1};
			bricks[5] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			bricks[6] = new int[9] {0,0,1, 0,1,0, 1,0,0};
			bricks[7] = new int[9] {0,1,0, 0,1,0, 0,1,0};
			bricks[8] = new int[9] {1,0,1, 0,1,0, 1,0,1};
			break;

		default:
			bricks[0] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			bricks[1] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			bricks[2] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			bricks[3] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			bricks[4] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			bricks[5] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			bricks[6] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			bricks[7] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			bricks[8] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			break;
		}

		// remove all bombs
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("bomb");
		foreach (GameObject go in gos) {
			Destroy(go);
		}
		
		// remove all old obstacles
		gos = GameObject.FindGameObjectsWithTag("obstacle");
		foreach (GameObject go in gos) {
			Destroy(go);
		}

		// 9 bricks, start at -5 bricks offset from origin (0,0)
		float left = -5f*offset.x;
		float y=-5f*offset.x, x=left;

		for(int i=0; i<9; i++) {
			x=left;
			y+=offset.y;

			for(int j=0; j<9; j++) {
				x+=offset.x;

				if(bricks[i][j] == 1) {
					brickCount++;
					Instantiate(brick, new Vector3(x, y, 0), Quaternion.identity);
				}
			}
		}
	}



	void buildObstacle(int level) {

		Vector2 offset = new Vector2(0.9f, 0.9f); // distance between bricks

		int[][] obstacles = new int[9][];
		
		switch (level) {

		case 0:
			obstacles[0] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[2] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[4] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[6] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[7] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 1:
			obstacles[0] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[2] = new int[9] {1,1,1, 0,0,0, 0,0,0};
			obstacles[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[4] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[6] = new int[9] {0,0,0, 0,0,0, 1,1,1};
			obstacles[7] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 2:
			obstacles[0] = new int[9] {0,0,1, 0,0,0, 0,0,0};
			obstacles[1] = new int[9] {0,0,1, 0,0,0, 0,0,0};
			obstacles[2] = new int[9] {0,0,1, 0,0,0, 0,0,0};
			obstacles[3] = new int[9] {0,0,1, 0,0,0, 0,0,0};
			obstacles[4] = new int[9] {1,1,1, 0,0,0, 0,0,0};
			obstacles[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[6] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[7] = new int[9] {0,0,0, 0,0,0, 0,1,0};
			obstacles[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 3:
			obstacles[0] = new int[9] {0,0,0, 0,1,1, 0,0,0};
			obstacles[1] = new int[9] {0,0,0, 0,1,0, 0,1,0};
			obstacles[2] = new int[9] {0,0,0, 1,0,0, 0,0,0};
			obstacles[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[4] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[5] = new int[9] {0,0,1, 0,0,0, 0,1,0};
			obstacles[6] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[7] = new int[9] {0,1,0, 0,0,1, 0,1,0};
			obstacles[8] = new int[9] {0,0,0, 0,0,1, 0,0,1};
			break;

		case 4:
			obstacles[0] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[2] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[3] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[4] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[6] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[7] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 5:
			obstacles[0] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[2] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[3] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[4] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[6] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[7] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[8] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			break;

		case 6:
			obstacles[0] = new int[9] {0,0,0, 0,0,0, 1,0,0};
			obstacles[1] = new int[9] {0,0,0, 0,0,0, 1,0,0};
			obstacles[2] = new int[9] {0,0,0, 0,0,0, 1,0,0};
			obstacles[3] = new int[9] {0,0,0, 0,0,0, 1,0,0};
			obstacles[4] = new int[9] {0,0,0, 0,0,0, 1,0,0};
			obstacles[5] = new int[9] {0,0,0, 0,0,0, 1,0,0};
			obstacles[6] = new int[9] {0,0,0, 0,0,0, 1,0,0};
			obstacles[7] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[8] = new int[9] {0,0,0, 0,0,0, 1,0,0};
			break;

		case 7:
			obstacles[0] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[1] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[2] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[3] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[4] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[5] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[6] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[7] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[8] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			break;

		case 8:
			obstacles[0] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[1] = new int[9] {0,0,0, 0,0,0, 0,1,0};
			obstacles[2] = new int[9] {0,0,0, 0,0,0, 0,1,0};
			obstacles[3] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[4] = new int[9] {0,0,0, 0,0,0, 0,0,0};
			obstacles[5] = new int[9] {0,1,0, 0,0,0, 0,0,0};
			obstacles[6] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[7] = new int[9] {0,1,0, 0,0,0, 0,0,0};
			obstacles[8] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			break;

		case 9:
			obstacles[0] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[1] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[2] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[3] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[4] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[5] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[6] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[7] = new int[9] {0,1,0, 0,1,0, 0,1,0};
			obstacles[8] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			break;

		default:
			obstacles[0] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[1] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[2] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[3] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[4] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[5] = new int[9] {0,1,0, 0,0,0, 0,1,0};
			obstacles[6] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			obstacles[7] = new int[9] {0,1,0, 0,1,0, 0,1,0};
			obstacles[8] = new int[9] {1,1,1, 1,1,1, 1,1,1};
			break;
		}
		
		// 9 bricks, start at -5 bricks offset from origin (0,0)
		float left = -5f*offset.x;
		float y=-5f*offset.x, x=left;

		GameObject parentObstacles = GameObject.Find("Obstacles");

		for(int i=0; i<9; i++) {
			x=left;
			y+=offset.y;
			
			for(int j=0; j<9; j++) {
				x+=offset.x;

				if(obstacles[i][j] == 1) {
					GameObject g = Instantiate(obstacle, new Vector3(x, y, 0), Quaternion.identity) as GameObject;
					g.transform.parent = parentObstacles.transform;
					g.GetComponent<obstacle>().setAsHorizontal(true);
				} else if(obstacles[i][j] == 2) {
					GameObject g = Instantiate(obstacle, new Vector3(x, y, 0), Quaternion.identity) as GameObject;
					g.transform.parent = parentObstacles.transform;
					g.GetComponent<obstacle>().setAsHorizontal(false);
				} 
			}
		}

	}
}	
